/**
 * Template Name: Eterna - v4.7.1
 * Template URL: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/
 * Author: BootstrapMade.com
 * License: https://bootstrapmade.com/license/
 */
(function () {
    "use strict";

    /**
     * Easy selector helper function
     */
    const select = (el, all = false) => {
        el = el.trim();
        if (all) {
            return [...document.querySelectorAll(el)];
        } else {
            return document.querySelector(el);
        }
    };

    /**
     * Easy event listener function
     */
    const on = (type, el, listener, all = false) => {
        let selectEl = select(el, all);
        if (selectEl) {
            if (all) {
                selectEl.forEach((e) => e.addEventListener(type, listener));
            } else {
                selectEl.addEventListener(type, listener);
            }
        }
    };

    /**
     * Easy on scroll event listener
     */
    const onscroll = (el, listener) => {
        el.addEventListener("scroll", listener);
    };

    /**
     * Scrolls to an element with header offset
     */
    const scrollto = (el) => {
        let header = select("#header");
        let offset = header.offsetHeight;

        if (!header.classList.contains("header-scrolled")) {
            offset -= 16;
        }

        let elementPos = select(el).offsetTop;
        window.scrollTo({
            top: elementPos - offset,
            behavior: "smooth",
        });
    };

    /**
     * Header fixed top on scroll
     */
    let selectHeader = select("#header");
    if (selectHeader) {
        let headerOffset = selectHeader.offsetTop;
        let nextElement = selectHeader.nextElementSibling;
        const headerFixed = () => {
            if (headerOffset - window.scrollY <= 0) {
                selectHeader.classList.add("fixed-top");
                nextElement.classList.add("scrolled-offset");
            } else {
                selectHeader.classList.remove("fixed-top");
                nextElement.classList.remove("scrolled-offset");
            }
        };
        window.addEventListener("load", headerFixed);
        onscroll(document, headerFixed);
    }

    /**
     * Back to top button
     */
    let backtotop = select(".back-to-top");
    if (backtotop) {
        const toggleBacktotop = () => {
            if (window.scrollY > 100) {
                backtotop.classList.add("active");
            } else {
                backtotop.classList.remove("active");
            }
        };
        window.addEventListener("load", toggleBacktotop);
        onscroll(document, toggleBacktotop);
    }

    /**
     * Mobile nav toggle
     */
    on("click", ".mobile-nav-toggle", function (e) {
        select("#navbar").classList.toggle("navbar-mobile");
        this.classList.toggle("bi-list");
        this.classList.toggle("bi-x");
    });

    /**
     * Mobile nav dropdowns activate
     */
    on(
        "click",
        ".navbar .dropdown > a",
        function (e) {
            if (select("#navbar").classList.contains("navbar-mobile")) {
                e.preventDefault();
                this.nextElementSibling.classList.toggle("dropdown-active");
            }
        },
        true
    );

    /**
     * Scrool with ofset on links with a class name .scrollto
     */
    on(
        "click",
        ".scrollto",
        function (e) {
            if (select(this.hash)) {
                e.preventDefault();

                let navbar = select("#navbar");
                if (navbar.classList.contains("navbar-mobile")) {
                    navbar.classList.remove("navbar-mobile");
                    let navbarToggle = select(".mobile-nav-toggle");
                    navbarToggle.classList.toggle("bi-list");
                    navbarToggle.classList.toggle("bi-x");
                }
                scrollto(this.hash);
            }
        },
        true
    );

    /**
     * Scroll with ofset on page load with hash links in the url
     */
    window.addEventListener("load", () => {
        if (window.location.hash) {
            if (select(window.location.hash)) {
                scrollto(window.location.hash);
            }
        }
    });

    /**
     * Hero carousel indicators
     */
    let heroCarouselIndicators = select("#hero-carousel-indicators");
    let heroCarouselItems = select("#heroCarousel .carousel-item", true);

    heroCarouselItems.forEach((item, index) => {
        index === 0
            ? (heroCarouselIndicators.innerHTML +=
                  "<li data-bs-target='#heroCarousel' data-bs-slide-to='" +
                  index +
                  "' class='active'></li>")
            : (heroCarouselIndicators.innerHTML +=
                  "<li data-bs-target='#heroCarousel' data-bs-slide-to='" +
                  index +
                  "'></li>");
    });

    /**
     * Clients Slider
     */
    new Swiper(".clients-slider", {
        speed: 400,
        loop: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        slidesPerView: 5,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el: ".swiper-pagination",
            type: "bullets",
            clickable: true,
        },
        breakpoints: {
            320: {
                slidesPerView: 2,
                spaceBetween: 40,
            },
            480: {
                slidesPerView: 3,
                spaceBetween: 60,
            },
            640: {
                slidesPerView: 4,
                spaceBetween: 80,
            },
            992: {
                slidesPerView: 5,
                spaceBetween: 100,
            },
        },
    });

    /**
     * Skills animation
     */
    let skilsContent = select(".skills-content");
    if (skilsContent) {
        new Waypoint({
            element: skilsContent,
            offset: "80%",
            handler: function (direction) {
                let progress = select(".progress .progress-bar", true);
                progress.forEach((el) => {
                    el.style.width = el.getAttribute("aria-valuenow") + "%";
                });
            },
        });
    }

    /**
     * Porfolio isotope and filter
     */
    window.addEventListener("load", () => {
        let portfolioContainer = select(".portfolio-container");
        if (portfolioContainer) {
            let portfolioIsotope = new Isotope(portfolioContainer, {
                itemSelector: ".portfolio-item",
                layoutMode: "fitRows",
            });

            let portfolioFilters = select("#portfolio-flters li", true);

            on(
                "click",
                "#portfolio-flters li",
                function (e) {
                    e.preventDefault();
                    portfolioFilters.forEach(function (el) {
                        el.classList.remove("filter-active");
                    });
                    this.classList.add("filter-active");

                    portfolioIsotope.arrange({
                        filter: this.getAttribute("data-filter"),
                    });
                },
                true
            );
        }
    });

    /**
     * Initiate portfolio lightbox
     */
    const portfolioLightbox = GLightbox({
        selector: ".portfolio-lightbox",
    });

    /**
     * Portfolio details slider
     */
    new Swiper(".portfolio-details-slider", {
        speed: 400,
        loop: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        pagination: {
            el: ".swiper-pagination",
            type: "bullets",
            clickable: true,
        },
    });

    document.addEventListener("DOMContentLoaded", function () {
        document
            .querySelectorAll(".sidebar .nav-link")
            .forEach(function (element) {
                element.addEventListener("click", function (e) {
                    let nextEl = element.nextElementSibling;
                    let parentEl = element.parentElement;

                    if (nextEl) {
                        e.preventDefault();
                        let mycollapse = new bootstrap.Collapse(nextEl);

                        if (nextEl.classList.contains("show")) {
                            mycollapse.hide();
                        } else {
                            mycollapse.show();
                            // find other submenus with class=show
                            var opened_submenu =
                                parentEl.parentElement.querySelector(
                                    ".submenu.show"
                                );
                            if(opened_submenu){
                                new bootstrap.Collapse(opened_submenu);
                                }
                        }
                    }
                });

            });

        $("table").addClass("styled-table");
        $("table").attr("id", "ticketLinesTable");
        $("#nav_accordion").find(".active").parent().parent().addClass("show");
        $("#nav_accordion")
            .find(".active")
            .parent()
            .parent()
            .parent()
            .find("a.nav-t")
            .addClass("active-parrent");
    });
    $("#nav_accordion").find("nav-t");

    $("#nav_accordion").find(".active").parent().parent().addClass("show");
    $("#nav_accordion")
        .find(".active")
        .parent()
        .parent()
        .parent()
        .find("a.nav-t")
        .addClass("active-parrent");
    if ($("#nav_accordion").find(".active").parent().parent().hasClass("show")
    ) {
        $("#nav_accordion")
            .find(".active")
            .parent()
            .parent()
            .parent()
            .find("i")
            .toggleClass("fa-angle-up");
        $("#nav_accordion")
            .find(".active")
            .parent()
            .parent()
            .parent()
            .find("i")
            .toggleClass("fa-angle-down");
    }
    $(".nav-t").on("click", function () {
        $(this).children().toggleClass("fa-angle-up");
        $(this).children().toggleClass("fa-angle-down");
    });

    $(".collapse")
        .on("shown.bs.collapse", function () {
            $(this)
                .parent()
                .find(".auto")
                .removeClass("fa-plus")
                .addClass("fa-minus");
        })
        .on("hidden.bs.collapse", function () {
            $(this)
                .parent()
                .find(".auto")
                .removeClass("fa-minus")
                .addClass("fa-plus");
        });

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="_token"]').attr("content"),
        },
    });
    $(".credential").on("click", function (e) {
        e.preventDefault();
        var credential = $(this).children().val();
        var cate = $(this).parent().parent().find(".category").val();
        var base_url = window.location.origin;
        var url =
            window.location.origin +
            "/" +
            cate +
            "/credential/" +
            credential +
            "#datables";
        window.history.replaceState(null, null, url);
        $.ajax({
            type: "GET",
            dataType: "json",
            url: url,
            cache: false,
            success: function (response) {
                var title = response[2].title;
                var img = "/img/credentials/" + response[2].images;
                var desc = response[2].description;
                var file = "/storage/credentials/" + response[2].file;
                $(".res-content").replaceWith(
                    '<div class="col-9 row res-content"><div class="col-5 img-col"><img src="' +
                        base_url +
                        img +
                        '"class="img-s" alt=""/></div><div class="col-7 pt-6 pt-lg-0 content-c" style="margin-top: 30px"><h2 class="tx-24 color-main fw-600">' +
                        title +
                        '</h2><div class="desc-content mb-3">' +
                        desc +
                        '</div><div><a class="btn-content" href="' +
                        base_url +
                        file +
                        '" target="_blank">Xem chi tiết</a>' +
                        "</div></div></div>"
                );
                $("a.btn-content").attr("href", file);
            },
        });
    });
    function view_more(x){
        var originalLength = x;
            var rowCount = $("table tr").length;

            var hidden = true;
            var table = $("table").find("tbody");

            table.find("tr:lt(" + rowCount + ")").hide();
            table.find("tr:lt(" + (originalLength - 1) + ")").show();

            $("#load_more").on("click", function (e) {
                e.preventDefault();
                if (hidden) {
                    // first on click, it is hidden, so expand
                    table.find("tr:lt(" + rowCount + ")").show();
                    $(this).html("Thu gọn"); //change html
                } else {
                    table.find("tr:lt(" + rowCount + ")").hide();
                    table.find("tr:lt(" + (originalLength - 1) + ")").show();
                    $(this).html("Xem thêm"); //change html
                }
                hidden = !hidden;
            });
    }

    function hiddenRow() {
        var originalLength = 30;

        var rowCount = $("table tr").length;

        var hidden = true;
        var table = $("table").find("tbody");

        table.find("tr:lt(" + rowCount + ")").hide();
        table.find("tr:lt(" + (originalLength - 1) + ")").show();
        if ($("table").height() < 750) {
                $("#load_more").css("display", "none");
        }

        if ( document.URL.includes("tvc#datables")){
            $("#load_more").css("display", "none");
        }

        if ( document.URL.includes("chi-tiet-bao-gia-ban-quyen#datables") ) {
            view_more(3);
        }


        if ( document.URL.includes("chi-tiet-bao-gia-y-te#datables") ) {
            view_more(7);
        }
        if ( document.URL.includes("chi-tiet-bao-gia-tin-tuc-bao-dai#datables") ) {
            view_more(5);
        }
        if ( document.URL.includes("chi-tiet-bao-gia-tik-tok#datables") ) {
            view_more(10);
        }

        if ( document.URL.includes("chi-tiet-bao-gia-phim-truong#datables") ) {
            view_more(2);
        }

        if ( document.URL.includes("chi-tiet-bao-gia-kols#datables") ) {
            view_more(9);
        }
    }
    hiddenRow();

    $(".quotation").on("click", function (e) {
        e.preventDefault();
        var quotation = $(this).children().val();
        var cate = $(this).parent().parent().find(".category").val();
        var base_url = window.location.origin;
        var url =
            window.location.origin +
            "/" +
            cate +
            "/quotation/" +
            quotation +
            "#datables";
        window.history.replaceState(null, null, url);

        $.ajax({
            type: "GET",
            dataType: "json",
            url: url,
            cache: false,
            success: function (response) {
                var name = response[2].name;
                var desc = response[2].description;
                var file = "/storage/quotation/" + response[2].file;
                $(".res-content").replaceWith(
                    '<div class="col-9 row res-content quot"><div class="col-12 pt-6 pt-lg-0 quotation content"><h2>' +
                        name +
                        '</h2><div class="desc-content mb-3"><div class="res-table">' +
                        desc +
                        '</div> <a id="load_more" class="load_more float-r mt-3">Xem thêm</a>   </div><div><a class="btn-content" href="' +
                        base_url +
                        file +
                        '" target="_blank">Xem chi tiết</a>' +
                        "</div></div></div>"
                );
                $("a.btn-content").attr("href", file);
                hiddenRow();
            },
        });
    });
    $("a.sub").on("click", function () {
        $("a.sub").removeClass("active");
        $("a.sub")
            .parent()
            .parent()
            .parent()
            .find("a.nav-t")
            .removeClass("active-parrent");

        $(this).addClass("active");
        $(this)
            .parent()
            .parent()
            .parent()
            .find("a.nav-t")
            .addClass("active-parrent");
    });
})();
