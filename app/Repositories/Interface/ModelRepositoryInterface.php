<?php

namespace App\Repositories\Interface;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class ModelRepositoryInterface.
 */
interface ModelRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function all();

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function show($id);
}
