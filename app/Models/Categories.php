<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Categories extends Model
{
    use HasFactory, Sluggable;

    protected $fillable = [
        'name',
        'created_by'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function user(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function credential(){
        return $this->hasMany(credentials::class,'category_id');
    }

    public function quotations(){
        return $this->hasOne(Quotations::class,'category_id');
    }

    public function listQuotations(){
        return $this->hasMany(ListQuotation::class,'category_id');
    }

}
