<?php

namespace App\Http\Controllers;

use App\Models\ListQuotation;
use App\Http\Requests\StoreListQuotationRequest;
use App\Http\Requests\UpdateListQuotationRequest;
use Illuminate\Http\Request;
use App\Models\Categories;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Model\ModelRepository;

class ListQuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $listQuotation, $category;

     public function __construct(ListQuotation $listQuotation, Categories $category)
     {
        $this->listQuotation = new ModelRepository($listQuotation);
        $this->category = new ModelRepository($category);
     }

    public function index()
    {
        //
        if(Auth::id() == 1){
            $listQuotation = $this->listQuotation->all();
            return view('admin.listquotation.index', ['listQuotation' => $listQuotation]);
        }
        else{
            $listQuotation = $this->listQuotation->getModel()->where('created_by', Auth::id())->get();
            return view('admin.listquotation.index', ['listQuotation' => $listQuotation]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = $this->category->all();
        return view('admin.listquotation.create',['categories'=>$categories,]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreListQuotationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreListQuotationRequest $request)
    {
        //
        if ($request->file('file')) {
            $file = $request->file('file');
            $file->store('public/quotation');
            $this->listQuotation->create([
                'name' => $request->name,
                'file' => $file->hashName(),
                'description' => $request->desc,
                'category_id' => $request->category,
                'created_by' => Auth::id()]);
            return back()->with('success', 'Add success!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ListQuotation  $listQuotation
     * @return \Illuminate\Http\Response
     */
    public function show(ListQuotation $listQuotation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ListQuotation  $listQuotation
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->authorize('update', $this->listQuotation->show($request->id));
        $list = $this->listQuotation->show($request->id);
        $categories = $this->category->all();
        return view('admin.listquotation.edit', ['list' => $list, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateListQuotationRequest  $request
     * @param  \App\Models\ListQuotation  $listQuotation
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateListQuotationRequest $request)
    {
        if ($request->file('file')) {

            $this->authorize('update', $this->listQuotation->show($request->id));
            $file = $request->file('file');
            $file->store('public/quotation');
            $this->listQuotation->update([
                'name' => $request->name,
                'total' => $request->total,
                'file' => $file->hashName(),
                'description' => $request->desc,
                'category_id' => $request->category,
            ], $request->id);
            return back()->with('success', 'Update success!');
        } else {

            $this->authorize('update', $this->listQuotation->show($request->id));
            $this->listQuotation->update([
                'name' => $request->name,
                'total' => $request->total,
                'file' => $this->listQuotation->show($request->id)->file,
                'description' => $request->desc,
                'category_id' => $request->category,
            ], $request->id);
            return back()->with('success', 'Update success!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ListQuotation  $listQuotation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->authorize('delete', $this->listQuotation->show($request->id));
        $this->listQuotation->delete($request->id);
        return back()->with('success', 'Delete success!');
    }
}
