<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\ListQuotation;
use App\Models\credentials;
use App\Models\Banner;

class HomepageController extends Controller
{
    //
    public function index()
    {
        $banners = Banner::all();
        $banner = Banner::orderBy('id', 'ASC')->first();
        $credential = credentials::orderBy('id', 'ASC')->first();
        $categories = Categories::with(['credential', 'listQuotations'])->orderBy('sort', 'ASC')->get();
        return view('welcome', ['categories' => $categories, 'credential' => $credential, 'banners' => $banners, 'banner' => $banner]);
    }

    public function category(Request $request)
    {
        $banners = Banner::all();
        $banner = Banner::orderBy('id', 'ASC')->first();
        $categories = Categories::orderBy('sort', 'ASC')->get();
        $category = Categories::where('id', $request->id)->first();
        $credentials = credentials::where('category_id', $category->id)->get();
        $listQuotation = ListQuotation::where('category_id', $category->id)->get();

        return view('frontend.category', [
            'categories' => $categories,
            'category' => $category,
            'credentials' => $credentials,
            'listQuotation' => $listQuotation,
            'banners' => $banners,
            'banner' => $banner
        ]);
    }

    public function credentials(Request $request)
    {
        if ($request->ajax()) {
            $banners = Banner::all();
            $banner = Banner::orderBy('id', 'ASC')->first();
            $credential = credentials::where('slug', $request->slug)->first();
            $categories = Categories::orderBy('sort', 'ASC')->get();
            return response()->json([$banners, $banner, $credential, $categories]);
        }else{
            $banners = Banner::all();
            $banner = Banner::orderBy('id', 'ASC')->first();
            $credential = credentials::where('slug', $request->slug)->first();
            $categories = Categories::orderBy('sort', 'ASC')->get();
            return view('frontend.credentials', ['categories' => $categories, 'credential' => $credential, 'banners' => $banners, 'banner' => $banner]);
        }
    }

    public function quotations(Request $request)
    {
        if ($request->ajax()) {
            $banners = Banner::all();
            $banner = Banner::orderBy('id', 'ASC')->first();
            $quotation = ListQuotation::where('slug', $request->slug)->first();
            $categories = Categories::orderBy('sort', 'ASC')->get();
            return response()->json([$banners, $banner, $quotation, $categories]);
        }
        else{
            $banners = Banner::all();
            $banner = Banner::orderBy('id', 'ASC')->first();
            $quotation = ListQuotation::where('slug', $request->slug)->first();
            $categories = Categories::orderBy('sort', 'ASC')->get();
            return view('frontend.quotations', ['categories' => $categories, 'quotation' =>$quotation, 'banners'=>$banners,'banner' =>$banner]);
        }
    }
}
