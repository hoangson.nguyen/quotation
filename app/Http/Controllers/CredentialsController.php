<?php

namespace App\Http\Controllers;

use App\Models\credentials;
use App\Models\Categories;
use App\Http\Requests\StorecredentialsRequest;
use App\Http\Requests\UpdatecredentialsRequest;
use App\Policies\CredentialsPolicy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Model\ModelRepository;

class CredentialsController extends Controller
{

    protected $credential, $category;

    public function __construct(credentials $credential, Categories $category)
    {
        $this->credential = new ModelRepository($credential);
        $this->category = new ModelRepository($category);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::id() == 1){
            $credentials = $this->credential->getModel()->orderBy('category_id','ASC')->get();
            return view('admin.credentials.index', ['credentials' => $credentials]);
        }
        else{
            $credentials = $this->credential->getModel()->where('created_by', Auth::id())->get();
            return view('admin.credentials.index', ['credentials' => $credentials]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $category = $this->category->all();
        return view('admin.credentials.create',['category'=> $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorecredentialsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorecredentialsRequest $request)
    {
        if($request->file('image') && $request->file('demo')){
            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file->move('img/credentials', $filename);

            $fileDemo = $request->file('demo');
            $fileDemo->store('public/credentials');

            $this->credential->create(['images'=>$filename,
                                 'description'=>$request->description,
                                 'title'=>$request->title,
                                 'category_id'=>$request->category,
                                 'file' => $fileDemo->hashName(),
                                 'created_by'=> Auth::id()]);
            return back()->with('success', 'Add success!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\credentials  $credentials
     * @return \Illuminate\Http\Response
     */
    public function show(credentials $credentials)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\credentials  $credentials
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->authorize('update', $this->credential->show($request->id));
        $credential = $this->credential->show($request->id);
        $category = $this->category->all();
        return view('admin.credentials.edit',['credential' => $credential,'category'=> $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatecredentialsRequest  $request
     * @param  \App\Models\credentials  $credentials
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatecredentialsRequest $request)
    {

        if($request->file('image') && $request->file('demo')){

            $this->authorize('update', $this->credential->show($request->id));

            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file->move('img/credentials', $filename);

            $fileDemo = $request->file('demo');
            $fileDemo->store('public/credentials');

            $this->credential->update(['images'=>$filename,
                                'description'=>$request->description,
                                'title'=>$request->title,
                                'category_id'=>$request->category,
                                'file' => $fileDemo->hashName()], $request->id);

            return back()->with('success', 'Update success!');
        }
        elseif($request->file('image')){

            $this->authorize('update', $this->credential->show($request->id));

            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file->move('img/credentials', $filename);

            $this->credential->update(['images'=>$filename,
                                'description'=>$request->description,
                                'title'=>$request->title,
                                'category_id'=>$request->category,
                                'file' => $this->credential->show($request->id)->file], $request->id);
            return back()->with('success', 'Update success!');
        }
        elseif($request->file('demo')){

            $this->authorize('update', $this->credential->show($request->id));

            $fileDemo = $request->file('demo');
            $fileDemo->store('public/credentials');

            $this->credential->update(['images'=>$this->credential->show($request->id)->images,
                                'description'=>$request->description,
                                'title'=>$request->title,
                                'category_id'=>$request->category,
                                'file' => $fileDemo->hashName()], $request->id);
            return back()->with('success', 'Update success!');

        }
        else{

            $this->authorize('update', $this->credential->show($request->id));

            $this->credential->update(['images'=>$this->credential->show($request->id)->images,
                                'description'=>$request->description,
                                'title'=>$request->title,
                                'category_id'=>$request->category,
                                'file' => $this->credential->show($request->id)->file], $request->id);
            return back()->with('success', 'Update success!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\credentials  $credentials
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->authorize('delete', $this->credential->show($request->id));

        $this->credential->delete($request->id);
        return back()->with('success', 'Delete success!');
    }
}
