<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function index()
    {
        $users = User::all();
        return view('admin.users.index',['users' => $users]);
    }


    public function destroy(Request $request)
    {
        //
        $user = User::findOrFail($request->id);
        $user->delete();
        return back()->with('success', 'Delete success!');
    }
}
