<?php

namespace App\Providers;

use App\Models\credentials;
use App\Models\ListQuotation;
use App\Policies\CredentialsPolicy;
use App\Policies\AdminPolicy;
use App\Policies\ListQuotationPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
       credentials::class => CredentialsPolicy::class,
       ListQuotation::class => ListQuotationPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('admin', [AdminPolicy::class, 'viewAdmin']);

        //
    }
}
