<?php

namespace App\Policies;

use App\Models\User;
use App\Models\credentials;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CredentialsPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->id == 1) {
            return true;
        }
    }
    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\credentials  $credentials
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, credentials $credentials)
    {
        //
        return true;
        
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\credentials  $credentials
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, credentials $credentials)
    {
        //
        return $user->id === $credentials->created_by
        ? Response::allow()
        : Response::deny('You do not own this Credential.');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\credentials  $credentials
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, credentials $credentials)
    {
        //
        return $user->id === $credentials->created_by
        ? Response::allow()
        : Response::deny('You do not own this Credential.');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\credentials  $credentials
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, credentials $credentials)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\credentials  $credentials
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, credentials $credentials)
    {
        //
    }
}
