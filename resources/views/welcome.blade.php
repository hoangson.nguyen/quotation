@extends('layouts.master')
@section('content')
        <!-- ======= Services Section ======= -->
        <!-- End Services Section -->

        <div class="col-9 row res-content">
            <div class="col-5 img-col">
                <img src="{{ asset('img/credentials') }}/{{ $credential?->images }}"
                    class="img-s" alt=""/>
            </div>
            <div class="col-7 pt-6 pt-lg-0 content-c" style="margin-top: 30px">
                <h2 class="tx-24 color-main fw-600">{{$credential->title}}</h2>
                <div class="desc-content mb-3">
                    {!! $credential?->description !!}
                </div>
                <div>
                       <a class="btn-content" href="{{ asset('storage/credentials') }}/{{ $credential->file }}" target="_blank">Xem chi tiết</a>
                </div>
            </div>
        </div>


    <!-- End #main -->


    <!-- Vendor JS Files -->
@endsection
