@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
@include('admin.components.header', [
    'title' => 'User',
])
@stop

@section('content')
<table class="table">
    <thead>
      <tr>
        <th scope="col"  class="w-10">ID</th>
        <th scope="col"  class="w-1 0">Name</th>
        <th>Email</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse($users as $key=>$user)
      <tr>
        <th scope="row">{{$key+1}}</th>
        <td>{{$user->name}}</td>
        <td class="width-desc"><p class="cut-text">{{$user->email}}</p></td>
        <td><a class="text-danger" href="#" data-user="{{$user->id}}" data-toggle="modal" data-target="#destroyUser">
            <i class="fas fa-minus"></i>
        </a></td>
      </tr>
      @empty
      <tr>
        <td colspan="3" class="text-center">No Data</td>
      </tr>
      @endforelse
    </tbody>
  </table>
  @include('admin.users.components._modal_destroyUser')
@stop

@section('js')
    <script>
        $('#destroyUser').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var data = button.data('user')
            var modal = $(this)
            modal.find('#user-id').val(data)
        })
    </script>
@endsection
