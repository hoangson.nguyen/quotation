    <div id="carouselExampleFade" class="carousel slide carousel-fade position-r" data-ride="carousel">
        <div class="position-a top-nav">
        <div class="container position-r">
            <div class="position-a w-100 d-flex justify-content-between" style="z-index:2">
                <div class="top-line">
                    <a href="/"><img class="logo" src="{{ asset('img/logo.png') }}" alt=""></a>
                </div>
                <div class="contact-top top-line top-info">
                    <img class="mr-4" src="{{asset('img/mail.png')}}"> <a style="color: white" href="mailto:info@mcv.com.vn">info@mcv.com.vn</a>
                    <i class="px-2"></i>
                    <img class="mr-4" src="{{asset('img/phone.png')}}"><a style="color: white" href="tel:02839105142">(+84) 28 3910 5142</a></i>
                </div>
            </div>
        </div>

        </div>

        <section>
            <div class="hero-container">
              <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-inner">
            @foreach ($banners as $key => $banner)
                <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                    <a href="{{$banner->link}}" target="_blank">
                    <div class="w-100">
                        <img class="banner" src='{{ asset('img/banner') }}/{{ $banner->images }}'/>
                    </div>
                    </a>
                </div>

            <ol class="carousel-indicators" id="hero-carousel-indicators" style="z-index: 100"></ol>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div></div></div></section>
