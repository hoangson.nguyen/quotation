<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <title>Công ty Cổ phần Tập đoàn MCV - MCV Group - MCV Corporation</title>
    <meta content="" name="description" />
    <meta content="" name="keywords" />

    <!-- Favicons -->
    <link rel="icon" href="https://mcv.com.vn/wp-content/uploads/2020/02/cropped-MCV-Group-tab-180x180.png" />
    <link href="{{ asset('img/skills-img.jpg') }}" rel="apple-touch-icon" />

    <!-- Google Fonts -->
    {{-- <link href="http://fonts.cdnfonts.com/css/roboto" rel="stylesheet"> --}}


    <!-- Vendor CSS Files -->
    <link href="{{ asset('vendor/animate.css/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Template Main CSS File -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" />

    <!-- =======================================================
  * Template Name: Eterna - v4.7.1
  * Template URL: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Top Bar ======= -->
    {{-- <section id="topbar" class="d-flex align-items-center">
        <div class="container d-flex fs-18">
            <div class="contact-info d-flex align-items-center">
                <a href="/"><img class="logo" src="{{ asset('img/logo.png') }}" alt=""></a>
                <i class="bi bi-envelope d-flex align-items-center"><a
                        href="mailto:contact@example.com">info@mcv.com.vn</a></i>
                <i class="bi bi-phone d-flex align-items-center ms-4"><span>+84 28 3910 5142</span></i>
            </div>
            <div class="flex-column1"></div>
            <div class="social-links d-none d-md-flex align-items-center">
                <a href="#" class="youtube"><i class="bi bi-youtube"></i></a>
                <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                <a href="#" class="tiktok"><i class="bi bi-tiktok"></i></a>
            </div>
        </div>
    </section> --}}
    @include('partials.menu')

    <section id="datables" class="datables">
        <div class="container">
            <div class="col-12 f-column">
                <div class="col-3 menu-data font-menu">
                    @if (Request::url() === route('homepage'))

                        <nav class="sidebar card py-2 mb-4">
                            <ul class="nav flex-column p-b-15" id="nav_accordion">
                                @foreach ($categories as $key=>$category)
                                    <li class="nav-item has-submenu">
                                        <a class="nav-link nav-t {{$key==0?"border-menu":''}}">
                                            <input type="hidden" class="category" value="{{ $category->slug }}">
                                            {{ $category->name }} <i style="float: right; line-height: 25px" class="fas fa-angle-down"></i>
                                        </a>
                                        <ul class="submenu collapse">
                                            @foreach ($category->credential as $item)
                                                <li class="credential" style="padding-left: 20px">
                                                    <input type="hidden" value="{{ $item->slug }}">
                                                    <a class="nav-link sub"
                                                        href="{{ route('frontend.credentials.index', ['cate' => $category->slug, 'slug' => $item->slug]) }}#datables">{{ $item->title }}
                                                    </a>
                                                </li>
                                            @endforeach
                                            @foreach ($category->listQuotations as $item)
                                                <li class="quotation" style="padding-left: 20px">
                                                    <input type="hidden" value="{{ $item->slug }}">
                                                    <a class="nav-link sub"
                                                        href="{{ route('frontend.quotations.index', ['category' => $category->slug, 'slug' => $item->slug]) }}#datables">{{ $item->name }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </nav>
                    @else
                        <nav class="sidebar card py-2 mb-4">
                            <ul class="nav flex-column p-b-15" id="nav_accordion">
                                @foreach ($categories as $key=>$category)
                                    @php
                                        $data = explode('/', url()->current());
                                    @endphp
                                    <li class="nav-item has-submenu">
                                        <a class="nav-link nav-t {{$key==0?"border-menu":''}}">
                                            <input type="hidden" class="category" value="{{ $category->slug }}">
                                            {{ $category->name }} <i style="float: right; line-height: 25px"
                                                class="fas fa-angle-down"></i>
                                        </a>
                                        <ul class="submenu collapse">
                                            @foreach ($category->credential as $item)
                                                <li class="credential" style="padding-left: 20px">
                                                    <input type="hidden" value="{{ $item->slug }}">
                                                    <a class="nav-link sub {{ $item->slug == $data['5'] ? 'active' : '' }}"
                                                        href="{{ route('frontend.credentials.index', ['cate' => $category->slug, 'slug' => $item->slug]) }}#datables">{{ $item->title }}
                                                    </a>
                                                </li>
                                            @endforeach
                                            @foreach ($category->listQuotations as $item)
                                                <li class="quotation" style="padding-left: 20px">
                                                    <input type="hidden" value="{{ $item->slug }}">
                                                    <a class="nav-link sub {{ $item->slug == $data['5'] ? 'active' : '' }}"
                                                        href="{{ route('frontend.quotations.index', ['category' => $category->slug, 'slug' => $item->slug]) }}#datables">{{ $item->name }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </nav>
                    @endif
                </div>
                @yield('content')

                {{-- <div id="services" class="services">
                            <div class="row">
                                <div class="col-4 creden-img">
                                    <img src="{{ asset('img/credentials') }}/{{ $credential?->images }}"
                                        class="img-credentials" alt="" />
                                </div>
                                <div class="col-4 pt-4 pt-lg-0 content">
                                    <div class="desc-content mb-3">
                                        {!! $credential?->description !!}
                                    </div>
                                    <div>
                                        <form action="{{ asset('storage/credentials') }}/{{ $credential->file }}"
                                            target="_blank">
                                            <button class="more" type="submit">Xem chi tiết</button>
                                        </form>
                                    </div>
                                </div>
                    </div> --}}
            </div>
        </div>
    </section>
    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients section-titles" style="margin-bottom: 104px; padding: 0px !important">
        <div class="container">
            <div class="section-titles">
                <h2 class="text-center fs-40 fw-400 mb-12">Đối tác</h2>
                <div class="clients-content position-r">
                    <p class="text-center fs-14 fw-400 mb-44">
                        {{-- Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit
                        sint consectetur velit. Quisquam quos quisquam cupiditate. --}}
                    </p>

                    <div class="clients-slider swiper clients-img">
                        <div class="swiper-wrapper align-items-center mb-5">
                            <div class="swiper-slide">
                                <img src="{{ asset('img/brand/acecook-logo.png') }}" class="img-fluid" alt="" />
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('img/brand/sharp_logo.png') }}" class="img-fluid" alt="" />
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('img/brand/doublemint_logo.png') }}" class="img-fluid" alt="" />
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('img/brand/logo-bibica.png') }}" class="img-fluid" alt="" />
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('img/brand/logo-msb.jpg') }}" class="img-fluid" alt="" />
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('img/brand/panasonic-logo.png') }}" class="img-fluid" alt="" />
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('img/brand/red-bull-Logo.png') }}" class="img-fluid" alt="" />
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('img/brand/toshiba-logo.png') }}" class="img-fluid" alt="" />
                            </div>
                            <div class="swiper-slide">
                                <img src="{{ asset('img/brand/yamaha-logo.png') }}" class="img-fluid" alt="" />
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>

                    <a class="swiper-button-prev" ></a>
                    <a class="swiper-button-next"></a>
                </div>
        </div>
    </section>
    {{-- <section id="clients1" class="section-titles">
        <div class="container">
            <div>
                <h2 class="text-center fs-40 fw-400 mb-56">FAQ</h2>
            </div>
            <div class="accordion" id="accordionExample">
                <div class="card n-border my-3 fs-18 fw-400">
                    <div class="mb-3" id="headingOne">
                        <h2 class="mb-0">
                            <div class="dot-acc"><i class="fas fa-circle fs-9"></i></div>
                            <button class="btn btn-link fs-18 color-acc" type="button" data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Anim pariatur cliche reprehenderit, en
                            </button>
                            <i class="auto plus-info fas fa-plus float-right" data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"></i>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                        data-parent="#accordionExample">
                        <div class="content-acc">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                            richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                            brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                            aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                            Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                            ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                            farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
                            accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="card n-border my-3 fs-18 fw-400">
                    <div class="mb-3" id="headingTwo">
                        <h2 class="mb-0">
                            <div class="dot-acc"><i class="fas fa-circle fs-9"></i></div>
                            <button class="btn btn-link fs-18 color-acc" type="button" data-toggle="collapse"
                                data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                Food truck quinoa nesciunt laborum eiusmod
                            </button>
                            <i class="auto plus-info fas fa-plus float-right" data-toggle="collapse"
                                data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo"></i>
                        </h2>
                    </div>

                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                        data-parent="#accordionExample">
                        <div class="content-acc">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                            richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                            brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                            aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                            Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                            ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                            farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
                            accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="card n-border my-3 fs-18 fw-400">
                    <div class="mb-3" id="headingThree">
                        <h2 class="mb-0">
                            <div class="dot-acc"><i class="fas fa-circle fs-9"></i></div>
                            <button class="btn btn-link fs-18 color-acc" type="button" data-toggle="collapse"
                                data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                Leggings occaecat craft beer farm-to-table
                            </button>
                            <i class="auto plus-info fas fa-plus float-right" data-toggle="collapse"
                                data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree"></i>
                        </h2>
                    </div>

                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                        data-parent="#accordionExample">
                        <div class="content-acc">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                            richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                            brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                            aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                            Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                            ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                            farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
                            accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="card n-border my-3 fs-18 fw-400">
                    <div class="mb-3" id="headingFour">
                        <h2 class="mb-0">
                            <div class="dot-acc"><i class="fas fa-circle fs-9"></i></div>
                            <button class="btn btn-link fs-18 color-acc" type="button" data-toggle="collapse"
                                data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                Anim pariatur cliche reprehenderit, en 1
                            </button>
                            <i class="auto plus-info fas fa-plus float-right" data-toggle="collapse"
                                data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour"></i>
                        </h2>
                    </div>

                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                        data-parent="#accordionExample">
                        <div class="content-acc">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                            richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                            brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                            aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                            Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                            ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                            farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
                            accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="card n-border my-3 fs-18 fw-400">
                    <div class="mb-3" id="headingFive">
                        <h2 class="mb-0">
                            <div class="dot-acc"><i class="fas fa-circle fs-9"></i></div>
                            <button class="btn btn-link fs-18 color-acc" type="button" data-toggle="collapse"
                                data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                Brunch 3 wolf moon tempor, sunt aliqua put a.
                            </button>
                            <i class="auto plus-info fas fa-plus float-right" data-toggle="collapse"
                                data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive"></i>
                        </h2>
                    </div>

                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                        data-parent="#accordionExample">
                        <div class="content-acc">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                            richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                            brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                            aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                            Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                            ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                            farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
                            accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
            </div>
    </section> --}}
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact section-titles">
        <div class="container">
            <div>
                <h2 class="text-center fs-40 fw-400">Liên hệ</h2>
            </div>
            <div class="row mt-5">
                <div class="col-4">
                    <div class="info-box mb-5 contact-card">
                        <img src="{{ asset('img/map-pin.svg') }}" alt="">
                        <h3>Địa chỉ</h3>
                        <p>
                            18Bis/22/1i đường Nguyễn Thị Minh Khai, Phường Đa Kao, Quận 1,
                            TP. HCM.
                        </p>
                    </div>
                </div>

                <div class="col-4">
                    <div class="info-box mb-5 contact-card">
                        <img src="{{ asset('img/mail.svg') }}" alt="">
                        <h3>Email</h3>
                        <a class="fs-14" style="color: #626262" href="mailto:info@mcv.com.vn">info@mcv.com.vn</a>
                    </div>
                </div>

                <div class="col-4">
                    <div class="info-box mb-5 contact-card">
                        <img src="{{ asset('img/phone.svg') }}" alt="">
                        <h3>Điện thoại</h3>
                        <a class="fs-14" style="color: #626262" href="tel:02839105142">(+84) 28 3910 5142</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-7">
                    <iframe class="mb-4 mb-lg-0 map-info"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.56760086703!2d106.7439958152371!3d10.767769792327428!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317525eb0958eb5d%3A0x22bce1f030083a29!2sMCV%20Complex!5e0!3m2!1sen!2s!4v1657529313928!5m2!1sen!2s"
                        frameborder="0" allowfullscreen></iframe>
                </div>

                <div class="col-lg-5">
                    <form action="" method="post" role="form" class="php-email-form">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="" class="title-form">Họ</label>
                                <input type="text" name="first-name" class="form-control" id="name"
                                    placeholder="Họ" required />
                            </div>
                            <div class="col-md-6 form-group mt-2 mt-md-0">
                                <label for="" class="title-form">Tên</label>
                                <input type="text" class="form-control" name="name" id="name"
                                    placeholder="Tên" required />
                            </div>
                        </div>
                        <div class="form-group mt-2">
                            <label for="" class="title-form">Email</label>
                            <input type="email" class="form-control" name="email" id="email"
                                placeholder="Email" required />
                        </div>
                        <div class="form-group mt-2">
                            <label for="" class="title-form">Điện thoại</label>
                            <input type="text" class="form-control" name="phone" id="phone"
                                placeholder="Điện thoại" required />
                        </div>
                        <div class="form-group mt-2">
                            <label for="" class="title-form">Câu hỏi thường gặp</label>
                            <select class="form-select" name="" id="">
                                <option value="Tôi có nhu cầu báo giá KOLs">Tôi có nhu cầu báo giá KOLs</option>
                                <option value="Tôi có nhu cầu tìm hiểu thêm về doanh nghiệp bạn">Tôi có nhu cầu tìm hiểu thêm về doanh nghiệp bạn</option>
                                <option value="Tôi có nhu cầu tài trợ và hợp tác các chương trình của MCV">Tôi có nhu cầu tài trợ và hợp tác các chương trình của MCV</option>
                                <option value="Tôi có nhu cầu hợp tác tổ chức event">Tôi có nhu cầu hợp tác tổ chức event</option>
                                <option value="Tôi có nhu cầu thuê phim trường của MCV">Tôi có nhu cầu thuê phim trường của MCV</option>
                            </select>
                        </div>
                        <div class="form-group mt-2">
                            <label for="" class="title-form">Tin nhắn</label>
                            <textarea class="form-control" name="message" rows="5" placeholder="Tin nhắn" required></textarea>
                        </div>
                        <div class="my-3">
                            <div class="loading">Loading</div>
                            <div class="error-message"></div>
                            <div class="sent-message">
                                Your message has been sent. Thank you!
                            </div>
                        </div>
                        <div class="text-center form-group">
                            <button type="submit" class="p-button">Gửi</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
        <div class="footer-bottom">© <?php echo date("Y") ?> MCV Group. All rights reserved. </div>
    </section>



    <!-- End Contact Section -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <script src="{{ asset('vendor/purecounter/purecounter_vanilla.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/glightbox/js/glightbox.min.js') }}"></script>
    {{-- <script src="{{ asset('vendor/isotope-layout/isotope.pkgd.min.js') }}"></script> --}}
    <script src="{{ asset('vendor/swiper/swiper-bundle.min.js') }}"></script>
    {{-- <script src="{{ asset('vendor/waypoints/noframework.waypoints.js') }}"></script> --}}
    {{-- <script src="{{ asset('vendor/php-email-form/validate.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js"
        integrity="sha512-6PM0qYu5KExuNcKt5bURAoT6KCThUmHRewN3zUFNaoI6Di7XJPTMoT6K0nsagZKk2OB4L7E3q1uQKHNHd4stIQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}

    <!-- Template Main JS File -->
    <script src="{{ asset('js/main.js') }}"></script>


</html>
