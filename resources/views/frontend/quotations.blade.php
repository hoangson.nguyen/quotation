@extends('layouts.master')
@section('content')
    <div class="col-9 row res-content quot">
        <div class="col-12 pt-6 pt-lg-0 content">
            <h2 for="">{{$quotation->name}}</h2>
            <div class="desc-content mb-3">
                <div class="res-table">
                    {!! $quotation?->description !!}
                    <a id="load_more" class="load_more float-r mt-3">Xem thêm</a>
                </div>
            </div>
            <div>
                <a class="btn-content" href="{{ asset('storage/quotation') }}/{{ $quotation->file }}" target="_blank">Xem chi tiết</a>

            </div>
        </div>
    </div>
@endsection
